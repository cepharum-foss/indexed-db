/**
 * Manages access on single database in current site's domain consisting of one
 * or more object stores.
 */
export class Database {
    /**
     * Open connection with database.
     *
     * If connection has been opened before that previous connection is
     * returned.
     *
     * @param dbName name of database to open
     * @param upgradeFn optional callback invoked to upgrade database
     * @returns promises connection with selected database
     */
    static open( dbName: string, upgradeFn?: (IDBVersionChangeEvent) => ( undefined | boolean ) ): Promise<Database>;

    /**
     * Close connection to database and dispose manager instance.
     *
     * @returns promises manager disconnected from database
     */
    close(): Promise<void>;

    /**
     * Checks if current database contains named object store.
     *
     * @param storeName name of object store to look up
     * @returns true if store exists in available database, false otherwise
     */
    hasObjectStore( storeName: string ): boolean;

    /**
     * Checks if given database contains named object store.
     *
     * This version is provided mostly for use in a versionchange event handler
     * e.g. on upgrading database. In most other cases you should stick with the
     * instance method @see Database#hasObjectStore().
     *
     * @param storeName name of object store to look up
     * @param database instance of database to use
     * @returns true if store exists in given database, false otherwise
     */
    static hasObjectStore( storeName: string, database?: IDBDatabase ): boolean;

    /**
     * Removes currently managed database including all contained stores.
     *
     * @returns promises database deleted
     */
    drop(): Promise<void>;

    /**
     * Detects current version of named database in context of running browser.
     *
     * @note This function relies on API features known to work in recent
     *       releases of Chrome, only, as of late 2019. You shouldn't use it in
     *       production code.
     *
     * @param dbName name of database to test
     * @returns promises version of named database existing in browser, NaN on a missing database
     */
    static getDatabaseVersion( dbName: string ): Promise<number>;
}


export interface StoreSchemaIndexedProperty {
    /**
     * Indicates if current property is to be used as primary key of records.
     */
    primary?: boolean;

    /**
     * Indicates if index of current property is unique thus rejecting multiple
     * records sharing same value in this property.
     */
    unique?: boolean;
}

export type StoreSchema = { [key: string]: StoreSchemaIndexedProperty };

/**
 * Manages single object store in a local database available to current site.
 */
export class Store {
    /**
     * Opens store in selected database.
     *
     * @param dbName name of database to open, "default" when omitted
     * @param storeName name of store in database to open, "default" when omitted
     * @param schema description of indexed properties per record in store
     * @returns promises manager for accessing prepared store
     */
    static open( dbName: string, storeName: string, schema: StoreSchema ): Promise<Store>;

    /**
     * Writes value into storage using provided key.
     *
     * @param key identifier of written value in storage
     * @param record set of properties to store in association with given key
     * @return promises current storage after writing value
     */
    write( key: ( string | string[] ), record: object ): Promise<Store>;

    /**
     * Fetches value from storage selected by its identifier.
     *
     * @param key identifier of value in storage
     * @param valueIfMissing value to provide if selected key is missing
     * @returns promises value read from database or given fallback if missing
     */
    read( key: ( string | string[] ), valueIfMissing?: any ): Promise<any>;

    /**
     * Fetches record selected by its ID and invokes provided callback for
     * adjusting fetched record before writing back to store.
     *
     * Provided callback may return `null` to request removal of provided
     * record from store.
     *
     * @note The whole cycle of reading record before invoking callback and
     *       writing it back to database isn't guaranteed to be covered by
     *       single transaction. A synchronous callback is expected to keep the
     *       transaction used to read the record before, though.
     *
     * @param key identifier of record to adjust
     * @param callback function invoked for returning adjusted version of provided record
     * @returns promises store on record adjusted
     */
    adjust( key: ( string | string[] ), callback: ( object, string ) => ( object | Promise<object> ) ): Promise<Store>;

    /**
     * Removes value from storage selected by its identifier.
     *
     * @param key identifier of value in storage
     * @returns promises current store after removing selected value
     */
    remove( key: ( string | string[] ) ): Promise<Store>;

    /**
     * Removes all records from managed store.
     *
     * @returns promises all records in managed store removed
     */
    clear(): Promise<Store>;

    /**
     * Removes managed store from database.
     *
     * @returns promises managed store removed
     */
    drop(): Promise<void>;

    /**
     * Fetches number of records in current store optionally limited to those
     * matching provided range.
     *
     * @param range optional range of records to count only
     * @param indexName name of index to apply range definition for matching records
     * @returns number of (matching) records
     */
    count( range?: IDBKeyRange, indexName?: string ): Promise<number>;

    /**
     * Lists records in store.
     *
     * @param options selection of record to be listed
     * @returns promises (excerpt of) matching records
     */
    list( options?: StoreListOptions ): Promise<StoreMatches>;

    /**
     * Lists records in store with indexed property per record exactly matching
     * given value.
     *
     * @param indexName name of indexed property to check per record
     * @param value value to be found in selected property of matching records
     * @param options selection of record to be listed
     * @returns promises list of records read from store
     */
    findMatching( indexName: string, value: any, options?: StoreSliceOptions ): Promise<StoreMatches>;

    /**
     * Lists records in store with indexed property per record having value in
     * given range of values.
     *
     * @param indexName name of indexed property to check per record
     * @param lower optional lower bound of range of values, range is always including any value given here
     * @param upper optional upper bound of range of values, range is always including any value given here
     * @param options selection of record to be listed and sorting customization
     * @returns promises list of records read from store
     */
    findInRange( indexName: string, lower: any, upper?: any, options?: StoreSortedSliceOptions ): Promise<StoreMatches>;
}

/**
 * Defines options for describing excerpt of records to fetch.
 */
export interface StoreSliceOptions {
    /**
     * number of matches to skip [default: 0]
     */
    offset?: number;

    /**
     * maximum number of matches to deliver [default: Infinity]
     */
    limit?: number;

    /**
     * true if result should include total number of matches [default: false]
     */
    count?: boolean;
}

/**
 * Defines options for describing excerpt of records to fetch.
 */
export interface StoreSortedSliceOptions extends StoreSliceOptions {
    /**
     * number of matches to skip [default: 0]
     */
    offset?: number;

    /**
     * maximum number of matches to deliver [default: Infinity]
     */
    limit?: number;

    /**
     * true if result should include total number of matches [default: false]
     */
    count?: boolean;

    /**
     * true for iterating over matching records in ascending order, false for descending order, undefined for any order [default: undefined]
     */
    ascending?: boolean;

    /**
     * true to get at most one record per value in indexed property [default: false]
     */
    distinct?: boolean;
}

/**
 * Defines options support on fetching records in a store.
 */
export interface StoreListOptions extends StoreSortedSliceOptions {
    /**
     * name of index that needs to match range [default: none]
     */
    indexName?: string;

    /**
     * description of range of values of indexed property [default: null, for matching any record in store]
     */
    range?: IDBKeyRange;
}

/**
 * Defines result from listing matching records in a store.
 */
export interface StoreMatches {
    /**
     * excerpt of matching records
     */
    items: StoreMatch[];

    /**
     * total count of matching records
     */
    total?: number;
}

/**
 * Defines single matching record in fetching records from store.
 */
export interface StoreMatch {
    /**
     * identifying key of matching record
     */
    key: any;

    /**
     * matching record's properties
     */
    record: object;
}
