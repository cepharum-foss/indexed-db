import { Database, Store } from "../../src/index.js";

const schema = {
	id: { primary: true },
	surname: {},
	givenName: {},
	born: {},
};

const records = [
	{ id: "ADR0018", surname: "Doe", givenName: "John", born: new Date( "1980-03-04 12:00:00" ) },
	{ id: "ADR0020", surname: "Bishop", givenName: "Jane", born: new Date( "1988-10-12 12:00:00" ) },
	{ id: "ADR0333", surname: "Smith", givenName: "Al", born: new Date( "1981-05-23 12:00:00" ) },
	{ surname: "Miller", givenName: "Joseph", born: new Date( "1986-09-18 12:00:00" ) },
	{ surname: "Bishop", givenName: "Melissa", born: new Date( "1983-01-08 12:00:00" ) },
];

describe( "A schema-based store", () => {
	let store;

	before( () => {
		return Database.open( "default" )
			.then( db => new Store( db, "addressbook", schema ) )
			.then( s => { store = s; } );
	} );

	before( () => new Promise( ( resolve, reject ) => {
		const write = index => {
			if ( index < records.length ) {
				const record = records[index];

				store.write( record.id ? null : "AUTO" + index, records[index] )
					.then( () => {
						record.id.should.not.be.null();

						setTimeout( write, 0, index + 1 );
					} )
					.catch( reject );
			} else {
				resolve();
			}
		};

		write( 0 );
	} ) );

	after( () => ( store ? store.drop() : null ) );

	describe( "provides basic access on single records such as", () => {
		it( "reading missing record", () => {
			return store.read( "ADR9999" ).should.be.Promise().which.is.resolvedWith( null );
		} );

		it( "reading existing record", () => {
			return store.read( "ADR0333" ).should.be.Promise().which.is.resolvedWith( {
				id: "ADR0333",
				surname: "Smith",
				givenName: "Al",
				born: new Date( "1981-05-23 12:00:00" ),
			} );
		} );

		it( "writing new record", () => {
			return store.write( "ADR5432", {
				surname: "Peters",
				givenName: "Susan",
				born: new Date( "1982-12-18 12:00:00" ),
			} ).should.be.Promise().which.is.resolvedWith( store );
		} );

		it( "reading previously created record", () => {
			return store.read( "ADR5432" ).should.be.Promise().which.is.resolvedWith( {
				id: "ADR5432",
				surname: "Peters",
				givenName: "Susan",
				born: new Date( "1982-12-18 12:00:00" ),
			} );
		} );

		it( "adjusting existing record via callback", () => {
			return store.adjust( "AUTO4", record => {
				record.givenName = "Roseanne";
				return record;
			} ).should.be.Promise().which.is.resolvedWith( store )
				.then( s => s.read( "AUTO4" ).should.be.resolvedWith( {
					id: "AUTO4",
					surname: "Bishop",
					givenName: "Roseanne",
					born: new Date( "1983-01-08 12:00:00" ),
				} ) );
		} );

		it( "over-writing existing record", () => {
			return store.write( "ADR0333", {
				surname: "Smith",
				givenName: "Albert",
				born: new Date( "1981-05-23 12:00:00" ),
			} ).should.be.Promise().which.is.resolvedWith( store );
		} );

		it( "reading previously over-written record", () => {
			return store.read( "ADR0333" ).should.be.Promise().which.is.resolvedWith( {
				id: "ADR0333",
				surname: "Smith",
				givenName: "Albert",
				born: new Date( "1981-05-23 12:00:00" ),
			} );
		} );

		it( "removing an existing record", () => {
			return store.remove( "ADR5432" ).should.be.Promise().which.is.resolvedWith( store );
		} );

		it( "reading previously removed record delivering `null`", () => {
			return store.read( "ADR5432" ).should.be.Promise().which.is.resolvedWith( null );
		} );
	} );

	describe( "can list existing records", () => {
		it( "unconditionally", () => {
			return store.list()
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( records.length );
					( total == null ).should.be.true();
				} );
		} );

		it( "limiting number of items delivered at most", () => {
			return store.list( { limit: 2 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 2 );
					( total == null ).should.be.true();
				} );
		} );

		it( "skipping some items", () => {
			return store.list( { offset: 2 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( records.length - 2 );
					( total == null ).should.be.true();
				} );
		} );

		it( "extracting particular slice", () => {
			return store.list( { offset: records.length - 1, limit: 5 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					( total == null ).should.be.true();
				} );
		} );

		it( "extracting particular slice while delivering total number of existing items", () => {
			return store.list( { offset: records.length - 1, limit: 5, count: true } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					total.should.be.Number().which.is.equal( records.length );
				} );
		} );
	} );

	describe( "can find records matching a selected property's value", () => {
		it( "listing all matches", () => {
			return store.findMatching( "surname", "Bishop" )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 2 );
					( total == null ).should.be.true();
				} );
		} );

		it( "limiting maximum number of matches", () => {
			return store.findMatching( "surname", "Bishop", { limit: 1 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					( total == null ).should.be.true();
				} );
		} );

		it( "skipping matches", () => {
			return store.findMatching( "surname", "Bishop", { offset: 1 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					( total == null ).should.be.true();
				} );
		} );

		it( "skipping and limiting matches", () => {
			return store.findMatching( "surname", "Bishop", { offset: 1, limit: 2 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					( total == null ).should.be.true();
				} );
		} );

		it( "skipping and limiting matches while providing total number of matches", () => {
			return store.findMatching( "surname", "Bishop", { offset: 1, limit: 2, count: true } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					total.should.be.Number().which.is.equal( 2 );
				} );
		} );
	} );

	describe( "can find records matching range of a selected property's value", () => {
		it( "listing all matches", () => {
			return store.findInRange( "givenName", "Jane", "Joseph" )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 3 );
					( total == null ).should.be.true();
				} );
		} );

		it( "limiting maximum number of matches", () => {
			return store.findInRange( "givenName", "Jane", "Joseph", { limit: 1 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					( total == null ).should.be.true();
				} );
		} );

		it( "skipping matches", () => {
			return store.findInRange( "givenName", "Jane", "Joseph", { offset: 1 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 2 );
					( total == null ).should.be.true();
				} );
		} );

		it( "skipping and limiting matches", () => {
			return store.findInRange( "givenName", "Jane", "Joseph", { offset: 1, limit: 1 } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					( total == null ).should.be.true();
				} );
		} );

		it( "skipping and limiting matches while providing total number of matches", () => {
			return store.findInRange( "givenName", "Jane", "Joseph", { offset: 2, limit: 2, count: true } )
				.then( ( { items, total } ) => {
					items.should.be.an.Array().which.has.length( 1 );
					total.should.be.Number().which.is.equal( 3 );
				} );
		} );
	} );
} );
