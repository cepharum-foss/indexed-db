/**
 * (c) 2019 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { Cache } from "../../src/cache.js";

describe( "Runtime cache", () => {
	describe( "exposes method has() which", () => {
		it( "requires one argument", () => {
			Cache.has.should.be.Function().which.has.length( 1 );
		} );

		it( "can be invoked with one argument", () => {
			Cache.has( "test" );
		} );

		it( "returns boolean", () => {
			Cache.has().should.be.Boolean();
			Cache.has( null ).should.be.Boolean();
			Cache.has( false ).should.be.Boolean();
			Cache.has( true ).should.be.Boolean();
			Cache.has( "" ).should.be.Boolean();
			Cache.has( "test" ).should.be.Boolean();
			Cache.has( 0 ).should.be.Boolean();
			Cache.has( 1 ).should.be.Boolean();
			Cache.has( ["test"] ).should.be.Boolean();
			Cache.has( { name: "test" } ).should.be.Boolean();
			Cache.has( () => "test" ).should.be.Boolean();
		} );
	} );

	describe( "exposes method read() which", () => {
		it( "requires one argument", () => {
			Cache.read.should.be.Function().which.has.length( 1 );
		} );

		it( "can be invoked with one argument", () => {
			Cache.read( "test" );
		} );

		it( "returns undefined on using invalid name for lookup", () => {
			( Cache.read() === undefined ).should.be.true();
			( Cache.read( null ) === undefined ).should.be.true();
			( Cache.read( false ) === undefined ).should.be.true();
			( Cache.read( true ) === undefined ).should.be.true();
			( Cache.read( "" ) === undefined ).should.be.true();
			( Cache.read( 0 ) === undefined ).should.be.true();
			( Cache.read( 1 ) === undefined ).should.be.true();
			( Cache.read( ["test"] ) === undefined ).should.be.true();
			( Cache.read( { name: "test" } ) === undefined ).should.be.true();
			( Cache.read( () => "test" ) === undefined ).should.be.true();
		} );

		it( "returns undefined on looking up missing record", () => {
			( Cache.read( "test" ) === undefined ).should.be.true();
		} );

		it( "returns previously written record on using same key as before", () => {
			const record = { database: null, ready: null, stores: new Map() };

			Cache.write( "test", record );
			Cache.read( "test" ).should.be.equal( record );
		} );
	} );

	describe( "exposes method write() which", () => {
		it( "requires two argument", () => {
			Cache.write.should.be.Function().which.has.length( 2 );
		} );

		it( "throws on providing invalid name in first argument", () => {
			const record = { database: null, ready: null, stores: new Map() };

			( () => Cache.write( undefined, record ) ).should.throw();
			( () => Cache.write( null, record ) ).should.throw();
			( () => Cache.write( false, record ) ).should.throw();
			( () => Cache.write( true, record ) ).should.throw();
			( () => Cache.write( "", record ) ).should.throw();
			( () => Cache.write( 0, record ) ).should.throw();
			( () => Cache.write( 1, record ) ).should.throw();
			( () => Cache.write( ["test"], record ) ).should.throw();
			( () => Cache.write( { name: "test" }, record ) ).should.throw();
			( () => Cache.write( () => "test", record ) ).should.throw();

			Cache.write( "proper-name", record );
		} );

		it( "throws on providing obviously invalid record in second argument", () => {
			( () => Cache.write( "proper-name", undefined ) ).should.throw();
			( () => Cache.write( "proper-name", null ) ).should.throw();
			( () => Cache.write( "proper-name", false ) ).should.throw();
			( () => Cache.write( "proper-name", true ) ).should.throw();
			( () => Cache.write( "proper-name", "" ) ).should.throw();
			( () => Cache.write( "proper-name", "value" ) ).should.throw();
			( () => Cache.write( "proper-name", 0 ) ).should.throw();
			( () => Cache.write( "proper-name", 1 ) ).should.throw();
			( () => Cache.write( "proper-name", ["value"] ) ).should.throw();
			( () => Cache.write( "proper-name", { name: "value" } ) ).should.throw();
			( () => Cache.write( "proper-name", () => "value" ) ).should.throw();

			( () => Cache.write( "proper-name", { database: null, ready: null } ) ).should.throw();
			( () => Cache.write( "proper-name", { database: null, stores: null } ) ).should.throw();
			( () => Cache.write( "proper-name", { ready: null, stores: null } ) ).should.throw();
			( () => Cache.write( "proper-name", { database: null } ) ).should.throw();
			( () => Cache.write( "proper-name", { stores: null } ) ).should.throw();
			( () => Cache.write( "proper-name", { ready: null } ) ).should.throw();

			Cache.write( "proper-name", { database: null, ready: null, stores: new Map() } );
		} );

		it( "returns cache manager for chaining calls", () => {
			Cache.write( "proper-name", { database: null, ready: null, stores: new Map() } ).should.be.equal( Cache );
		} );
	} );

	describe( "exposes method remove() which", () => {
		it( "requires one argument", () => {
			Cache.remove.should.be.Function().which.has.length( 1 );
		} );

		it( "throws on providing invalid name in first argument", () => {
			( () => Cache.remove( undefined ) ).should.throw();
			( () => Cache.remove( null ) ).should.throw();
			( () => Cache.remove( false ) ).should.throw();
			( () => Cache.remove( true ) ).should.throw();
			( () => Cache.remove( "" ) ).should.throw();
			( () => Cache.remove( 0 ) ).should.throw();
			( () => Cache.remove( 1 ) ).should.throw();
			( () => Cache.remove( ["test"] ) ).should.throw();
			( () => Cache.remove( { name: "test" } ) ).should.throw();
			( () => Cache.remove( () => "test" ) ).should.throw();

			Cache.remove( "proper-name" );
		} );

		it( "returns cache manager for chaining calls", () => {
			Cache.remove( "proper-name" ).should.be.equal( Cache );
		} );

		it( "removes previously set record from cache", () => {
			const record = { database: null, ready: null, stores: new Map() };

			Cache.write( "some-test", record );
			Cache.read( "some-test" ).should.be.equal( record );
			Cache.remove( "some-test" );
			( Cache.read( "some-test" ) === undefined ).should.be.true();
		} );
	} );
} );
