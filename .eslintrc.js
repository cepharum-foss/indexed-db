module.exports = {
    extends: "eslint-config-cepharum",
    parserOptions: {
        sourceType: "module"
    },
    env: {
        browser: true,
        jest: true,
    },
    globals: {
        page: true,
        browser: true,
        context: true,
        jestPuppeteer: true
    }
};
