#!/bin/sh

if [ "$1" == "run" ]; then
	mkdir -p /home/chrome/opt
	apk add --no-cache findutils

	cd /home/chrome/app && find -name node_modules -prune -o -name .git -prune -o -print | cpio -p /home/chrome/opt
	cd /home/chrome/opt && npm ci && npm run test:ci
else
	docker run -it --rm -v "/$(pwd):/home/chrome/app:ro" -u root -w //home/chrome/app cepharum/e2e-chrome-ci ./ci-test.sh run
fi
