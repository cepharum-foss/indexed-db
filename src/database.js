/**
 * (c) 2018 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { Cache } from "./cache.js";

/**
 * Manages single IndexedDB instance of current browser.
 */
class Database {
	/**
	 * @param {string} databaseName name of database to manage
	 * @param {function(IDBVersionChangeEvent):boolean=} upgradeFn callback invoked to upgrade provided database
	 * @throws Error on missing mature support for IndexedDB in browser
	 */
	constructor( databaseName, upgradeFn = null ) {
		if ( !this.constructor.api ) {
			throw new Error( "FATAL ERROR: your browser does not feature IndexedDB" );
		}

		const dbName = typeof databaseName === "string" ? databaseName.trim() : "";
		if ( dbName === "" ) {
			throw new TypeError( "invalid database name" );
		}


		Object.defineProperties( this, {
			/**
			 * Provides name of database managed by current instance.
			 *
			 * @name Database#name
			 * @property string
			 * @readonly
			 */
			name: { value: dbName },
		} );

		let _version = 0;
		let ready = Promise.resolve( this );
		let connection = null;

		Object.defineProperties( this, {
			/**
			 * Delivers existing database instance.
			 *
			 * This property may be null when connection hasn't been established
			 * yet or manager has disconnected from database (temporarily). You
			 * should rely on Database#ready if your code relies on existing
			 * connection with database.
			 *
			 * @name Database#connection
			 * @property {?IDBDatabase}
			 * @readonly
			 */
			connection: {
				get: () => connection,
				set: value => {
					if ( value != null ) {
						throw new Error( "must not assign database connection" );
					}

					connection = null;

					this.ready = null;
				},
			},

			/**
			 * Promises manager established connection with database.
			 *
			 * When database was having issues with connecting previously, this
			 * promise keeps failing unless resetting it by assigning nullish
			 * value. Assigning any other value results in exception thrown.
			 *
			 * @name Database#ready
			 * @property {?Promise<Database>}
			 */
			ready: {
				get: () => ready,
				set: newState => {
					if ( newState == null ) {
						ready = ready ? ready.catch( error => {
							console.warn( `dropping issue with IndexedDB ${this.name}: ${error.message}` );

							return this;
						} ) : Promise.resolve( this );
					} else if ( newState instanceof Promise ) {
						ready = ready.then( () => newState );
					} else {
						throw new TypeError( "invalid state transition" );
					}
				},
			},

			/**
			 * Triggers (re-)connection to database by assigning custom upgrade
			 * handler.
			 *
			 * @name Database#upgradeHandler
			 * @property {function(IDBVersionChangeEvent):boolean}
			 */
			upgradeHandler: {
				get: () => { throw new Error( "upgrade handler can't be read" ); },
				set: newUpgradeFn => {
					const fn = typeof newUpgradeFn === "function" ? newUpgradeFn : null;

					if ( connection && !fn ) {
						// already connected and nothing to upgrade -> leave it like that
						return;
					}

					this.ready = new Promise( ( resolve, reject ) => {
						// drop any existing connection
						if ( connection ) {
							connection.close();
							connection = null;
						}

						// establish (another) connection
						const request = _version > 0 ? Database.api.open( dbName, _version + 1 ) : Database.api.open( dbName );

						if ( fn ) {
							request.addEventListener( "upgradeneeded", event => {
								// requested connection is entitled to upgrade database schema now
								_version = event.newVersion;

								let result;

								try {
									result = fn( event, request.result, request.transaction );
								} catch ( error ) {
									reject( error );
								}

								if ( result instanceof Promise ) {
									result.catch( reject );
									result = undefined;
								}

								return result;
							} );
						}

						request.addEventListener( "error", reject );

						request.addEventListener( "success", () => {
							// connection has been (upgraded and) established
							const newConnection = request.result;

							_version = newConnection.version;

							connection = newConnection;

							// connection lost due to external event (such as local storage reset by user)
							connection.addEventListener( "close", () => {
								if ( connection === newConnection ) {
									connection = null;
									this.ready = null;
								}
							} );

							// connection is blocking request for upgrading database schema -> disconnect
							connection.addEventListener( "versionchange", event => {
								_version = event.newVersion;

								newConnection.close();

								if ( newConnection === connection ) {
									connection = null;
									this.ready = null;
								}
							} );

							resolve( this );
						} );

						request.addEventListener( "blocked", event => {
							const cached = Cache.read( this.name );
							const isTracked = cached && cached.database === this;

							console.warn( `${isTracked ? "tracked" : "UNTRACKED"} request for opening database ${dbName} got blocked:`, event );
						} );
					} );
				},
			},
		} );

		// trigger connection established with database (incl. optional upgrade of its schema)
		this.upgradeHandler = upgradeFn;
	}

	/**
	 * Open connection with database.
	 *
	 * If connection has been opened before that previous connection is
	 * returned.
	 *
	 * @param {string} dbName name of database to open
	 * @param {function(IDBVersionChangeEvent):boolean=} upgradeFn optional callback invoked to upgrade database
	 * @returns {Promise<Database>} promises connection with selected database
	 */
	static open( dbName = "default", upgradeFn = null ) {
		const _dbName = typeof dbName === "string" ? dbName.trim() : "";
		if ( _dbName === "" ) {
			return Promise.reject( new TypeError( "invalid database name" ) );
		}

		const fn = typeof upgradeFn === "function" ? upgradeFn : null;

		let cached = Cache.read( _dbName );
		if ( cached ) {
			if ( cached.deleted ) {
				return cached.deleted
					.then( () => this.open( _dbName, upgradeFn ) );
			}

			if ( fn && fn !== cached.upgradeFn ) {
				// assign upgrade handler -> will adjust cached.ready
				cached.database.upgradeHandler = fn;

				Object.defineProperty( cached, "upgradeFn", {
					value: fn,
					configurable: true,
				} );
			}
		} else {
			let db;

			try {
				db = new this( _dbName, upgradeFn );
			} catch ( error ) {
				return Promise.reject( error );
			}

			cached = {};

			Object.defineProperties( cached, {
				upgradeFn: { value: fn, configurable: true },
				database: { value: db, enumerable: true },
				stores: { value: new Map(), enumerable: true },
				ready: { get: () => db.ready, enumerable: true },
			} );

			Cache.write( _dbName, cached );
		}

		return cached.database.ready;
	}

	/**
	 * Close connection to database and dispose manager instance.
	 *
	 * @returns {Promise} promises manager disconnected from database
	 */
	close() {
		const cached = Cache.read( this.name );
		if ( cached && cached.database === this ) {
			Cache.remove( this.name );
		}

		return this.ready
			.then( () => {
				if ( this.connection ) {
					this.connection.close();
					this.connection = null;
				}
			} );
	}

	/**
	 * Checks if current database contains named object store.
	 *
	 * @param {string} storeName name of object store to look up
	 * @returns {boolean} true if store exists in available database, false otherwise
	 */
	hasObjectStore( storeName ) {
		return this.constructor.hasObjectStore( storeName, this.connection );
	}

	/**
	 * Checks if given database contains named object store.
	 *
	 * This version is provided mostly for use in a versionchange event handler
	 * e.g. on upgrading database. In most other cases you should stick with the
	 * instance method @see Database#hasObjectStore().
	 *
	 * @param {string} storeName name of object store to look up
	 * @param {?IDBDatabase} database instance of database to use
	 * @returns {boolean} true if store exists in given database, false otherwise
	 */
	static hasObjectStore( storeName, database ) {
		if ( database == null ) {
			return false;
		}

		if ( !( database instanceof IDBDatabase ) ) {
			throw new Error( "invalid database" );
		}

		const existingStores = database.objectStoreNames;
		const numStores = existingStores.length;

		for ( let i = 0; i < numStores; i++ ) {
			if ( existingStores[i] === storeName ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Closes all currently cached connections with a database.
	 *
	 * @returns {Promise<*>} promises all databases disconnected
	 */
	static shutDown() {
		const connections = [];

		for ( const entry of Cache._data.values() ) {
			connections.push( entry.database.close() );
		}

		return Promise.all( connections );
	}

	/**
	 * Removes currently managed database including all contained stores.
	 *
	 * @returns {Promise} promises database deleted
	 */
	drop() {
		const name = this.name;
		const cache = Cache.read( name );

		if ( !cache ) {
			return Promise.resolve();
		}

		if ( !cache.deleted ) {
			const database = cache.database;

			database.ready = null;
			database.ready = new Promise( ( resolve, reject ) => {
				if ( this.connection ) {
					this.connection.close();
					this.connection = null;
				}

				const request = Database.api.deleteDatabase( name );

				request.onerror = reject;
				request.onsuccess = () => {
					Cache.remove( name );
					resolve();
				};
			} );

			cache.deleted = database.ready;
		}

		return cache.deleted;
	}

	/**
	 * Detects current version of named database in context of running browser.
	 *
	 * @note This function relies on API features known to work in recent
	 *       releases of Chrome, only, as of late 2019. You shouldn't use it in
	 *       production code.
	 *
	 * @param {string} dbName name of database to test
	 * @returns {Promise<int>} promises version of named database existing in browser, NaN on a missing database
	 */
	static getDatabaseVersion( dbName ) {
		console.warn( "Database.getDatabaseVersion() is expected to work in Chrome, only." );

		return this.api.databases()
			.then( databases => {
				const numDatabase = databases.length;

				for ( let i = 0; i < numDatabase; i++ ) {
					const { name, version } = databases[i];
					if ( name === dbName ) {
						return version;
					}
				}

				return NaN;
			} );
	}
}

Object.defineProperties( Database, {
	/**
	 * Exposes API of browser for basically working with IndexedDBs.
	 *
	 * @name Database.api
	 * @property {IDBFactory}
	 * @readonly
	 */
	api: { value: window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB },

	/**
	 * Exposes common runtime cache.
	 *
	 * @name Database._cache
	 * @property {Map<string,object>}
	 * @readonly
	 * @protected
	 */
	_cache: { value: Cache },
} );

export { Database };
