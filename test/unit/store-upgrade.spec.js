import { Store } from "../../src/index.js";

describe( "Store", () => {
	beforeEach( () => Store.open( "my-custom-db", "my-custom-store" ).then( store => store.drop() ) );
	after( () => Store.open( "my-custom-db", "my-custom-store" ).then( store => store.database.close() ) );

	it( "can be re-opened with extended schema", () => {
		return Store.open( "my-custom-db", "my-custom-store", {
			name: { primary: true },
		} )
			.then( store => store.database.close() )
			.then( () => Store.open( "my-custom-db", "my-custom-store", {
				name: { primary: true },
				usable: {},
				portable: {},
			} )
				.then( store => store.write( "test", { name: "test", usable: [ 1, 2, 3 ], portable: { a: 1, b: 2, c: 3 } } ) )
				.then( store => store.list() )
				.then( list => {
					list.items.should.be.Array().which.has.length( 1 );
				} )
			);
	} );

	it( "can be re-opened with reduced schema", () => {
		return Store.open( "my-custom-db", "my-custom-store", {
			name: { primary: true },
			usable: {},
			portable: {},
		} )
			.then( store => store.database.close() )
			.then( () => Store.open( "my-custom-db", "my-custom-store", {
				name: { primary: true },
			} )
				.then( store => store.write( "test", { name: "test", usable: [ 1, 2, 3 ], portable: { a: 1, b: 2, c: 3 } } ) )
				.then( store => store.list() )
				.then( list => {
					list.items.should.be.Array().which.has.length( 1 );
				} )
			);
	} );

	it( "can be re-opened with completely different schema", () => {
		return Store.open( "my-custom-db", "my-custom-store", {
			first: { primary: true },
			usable: {},
			portable: {},
		} )
			.then( store => store.database.close() )
			.then( () => Store.open( "my-custom-db", "my-custom-store", {
				second: { primary: true },
				major: {},
				minor: {},
				extra: {},
			} )
				.then( store => store.write( "test", { first: "test", major: [ 1, 2, 3 ], minor: true, extra: 42 } ) )
				.then( store => store.list() )
				.then( list => {
					list.items.should.be.Array().which.has.length( 1 );
				} )
			);
	} );

	it( "can be opened w/ and w/o upgrade simultaneously", () => {
		return Promise.all( [
			Store.open( "my-custom-db", "my-custom-store", {
				name: { primary: true },
				usable: {},
				portable: {},
			} ),
			new Promise( resolve => setTimeout( resolve, 50 ) ).then( () => Store.open( "my-custom-db", "my-custom-store", {
				name: { primary: true },
				usable: {},
				portable: {},
			} ) ),
		] )
			.then( ( [ store1, store2 ] ) => Promise.all( [
				store1.list(),
				store2.list()
			] ) );
	} );
} );
