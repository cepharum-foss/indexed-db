const Path = require( "path" );

module.exports = function( config ) {
	config.set( {
		frameworks: [
			"mocha",
			"should",
		],

		files: [
			// tests
			{ pattern: "test/unit/**/*.spec.js", type: "module" },
			// files tests rely on
			{ pattern: "src/**/*.js", type: "module", included: false },
		],

		reporters: [ "spec", "coverage-istanbul" ],

		browsers: ["ChromeHeadless"],

		customLaunchers: {
			ChromeHeadlessNoSandbox: {
				base: "ChromeHeadless",
				flags: ["--no-sandbox"],
			},
		},

		preprocessors: {
			"src/**/!(*.spec).js": ["karma-coverage-istanbul-instrumenter"]
		},

		coverageIstanbulInstrumenter: {
			esModules: true
		},

		singleRun: true,

		coverageIstanbulReporter: {
			reports: [ "html", "text" ],
			dir: Path.join( __dirname, "coverage" ),
		},
	} );
};
