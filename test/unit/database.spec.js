import { Database } from "../../src/index.js";

describe( "Class Database", () => {
	describe( "has a constructor which", () => {
		it( "requires first argument to be non-empty name of database as string", () => {
			( () => new Database( undefined ) ).should.throw();
			( () => new Database( null ) ).should.throw();
			( () => new Database( false ) ).should.throw();
			( () => new Database( true ) ).should.throw();
			( () => new Database( "" ) ).should.throw();
			( () => new Database( 1 ) ).should.throw();
			( () => new Database( () => "default" ) ).should.throw();
			( () => new Database( ["default"] ) ).should.throw();
			( () => new Database( { name: "default" } ) ).should.throw();

			return new Database( "default" )
				// close manually for interfering with cache of Database.open()
				.close();
		} );
	} );

	describe( "has method open() which", () => {
		it( "can be invoked without argument", () => {
			return Database.open().should.be.Promise().which.is.resolved()
				.then( result => result.should.be.instanceOf( Database ) );
		} );

		it( "can be invoked with one argument", () => {
			return Database.open( "different-db" ).should.be.Promise().which.is.resolved()
				.then( result => result.should.be.instanceOf( Database ) );
		} );

		it( "can be invoked with two arguments", () => {
			return Database.open( "different-db", () => {} ).should.be.Promise().which.is.resolved()
				.then( result => result.should.be.instanceOf( Database ) );
		} );

		it( "requires non-empty name of database provided as string in first argument", () => {
			return Database.open( "" ).should.be.Promise().which.is.rejected()
				.then( () => Database.open( false ).should.be.Promise().which.is.rejected() )
				.then( () => Database.open( true ).should.be.Promise().which.is.rejected() )
				.then( () => Database.open( 0 ).should.be.Promise().which.is.rejected() )
				.then( () => Database.open( 1 ).should.be.Promise().which.is.rejected() )
				.then( () => Database.open( () => "test" ).should.be.Promise().which.is.rejected() )
				.then( () => Database.open( ["test"] ).should.be.Promise().which.is.rejected() )
				.then( () => Database.open( { name: "test" } ).should.be.Promise().which.is.rejected() );
		} );

		it( "always provides same promise when invoked without argument", () => {
			return Promise.all( [
				Database.open(),
				Database.open(),
				Database.open(),
			] )
				.then( ( [ a, b, c ] ) => {
					a.should.be.equal( b );
					b.should.be.equal( c );
				} );
		} );

		it( "always provides same promise when invoked with same sole argument", () => {
			return Promise.all( [
				Database.open( "different-db" ),
				Database.open( "different-db" ),
				Database.open( "different-db" ),
			] )
				.then( ( [ a, b, c ] ) => {
					a.should.be.equal( b );
					b.should.be.equal( c );
				} );
		} );

		it( "provides different promises when invoked with different sole argument", () => {
			return Promise.all( [
				Database.open( "different-db-1" ),
				Database.open( "different-db-2" ),
				Database.open( "different-db-3" ),
			] )
				.then( ( [ a, b, c ] ) => {
					a.should.not.be.equal( b );
					b.should.not.be.equal( c );
					c.should.not.be.equal( a );
				} );
		} );

		it( "always provides same promise when invoked with same pair of arguments", () => {
			const fn = () => {};

			return Promise.all( [
				Database.open( "different-db", fn ),
				Database.open( "different-db", fn ),
				Database.open( "different-db", fn ),
			] )
				.then( ( [ a, b, c ] ) => {
					a.should.be.equal( b );
					b.should.be.equal( c );
				} );
		} );

		it( "promises same database manager even when providing different upgrade handlers in second argument", () => {
			const fn = () => {};

			return Promise.all( [
				Database.open( "different-db-1", fn ),
				Database.open( "different-db-2", fn ),
				Database.open( "different-db-3", fn ),
				Database.open( "different-db", () => {} ),
				Database.open( "different-db", () => {} ),
				Database.open( "different-db", () => {} ),
			] )
				.then( ( [ a, b, c, d, e, f ] ) => {
					a.should.not.be.equal( b );
					b.should.not.be.equal( c );
					c.should.not.be.equal( a );

					d.should.be.equal( e );
					e.should.be.equal( f );
				} );
		} );

		it( "succeeds when (re-)opening database right after starting request to delete it", () => {
			return Database.open( "some-db-to-delete" )
				.then( db => {
					db.drop();

					return Database.open( "some-db-to-delete" )
						.then( _db => {
							_db.should.not.be.equal( db );
						} );
				} );
		} );

		it( "rejects when upgrade handler throws", () => {
			return Database.open( "some-db-to-delete", () => {
				throw new Error( "upgrade had some issue" );
			} ).should.be.Promise().which.is.rejected();
		} );

		describe( "promises instance for accessing database which", () => {
			it( "exposes promise for database being ready", () => {
				return Database.open().then( db => {
					db.ready.should.be.Promise();
				} );
			} );

			it( "rejects promise for database being ready replaced with truthy value", () => {
				return Database.open().then( db => {
					( () => { db.ready = true; } ).should.throw();
					( () => { db.ready = 1; } ).should.throw();
					( () => { db.ready = ""; } ).should.throw();
					( () => { db.ready = "test"; } ).should.throw();
					( () => { db.ready = () => {}; } ).should.throw();
				} );
			} );

			it( "rejects promise for database being ready replaced with falsy value", () => {
				return Database.open().then( db => {
					( () => { db.ready = false; } ).should.throw();
					( () => { db.ready = ""; } ).should.throw();
					( () => { db.ready = 0; } ).should.throw();
				} );
			} );

			it( "accepts promise for database being ready replaced with nullish value (resetting its state)", () => {
				return Database.open().then( db => {
					( () => { db.ready = null; } ).should.not.throw();
					( () => { db.ready = undefined; } ).should.not.throw();
				} );
			} );

			it( "exposes IDBDatabase instance for working with connected database", () => {
				return Database.open().then( db => {
					db.connection.should.be.instanceOf( IDBDatabase );
				} );
			} );

			it( "rejects to replace exposed IDBDatabase value with a truthy value", () => {
				return Database.open().then( db => {
					( () => { db.connection = true; } ).should.throw();
					( () => { db.connection = 1; } ).should.throw();
					( () => { db.connection = ""; } ).should.throw();
					( () => { db.connection = "test"; } ).should.throw();
					( () => { db.connection = () => {}; } ).should.throw();
				} );
			} );

			it( "rejects to replace exposed IDBDatabase value with a false", () => {
				return Database.open().then( db => {
					( () => { db.connection = false; } ).should.throw();
				} );
			} );

			it( "accepts to replace exposed IDBDatabase value with null or undefined to drop the connection", () => {
				return Database.open().then( db => {
					db.connection.close(); // need to disconnect manually first for dropping instance below
					Database._cache.remove( db.name );

					( () => { db.connection = null; } ).should.not.throw();
					( () => { db.connection = undefined; } ).should.not.throw();
				} );
			} );

			it( "accepts replacing upgrade handler causing reconnection and upgrade of database implicitly", () => {
				return Database.open().then( db => {
					const version = db.connection.version;

					db.upgradeHandler = () => {};

					return db.ready.then( () => {
						db.connection.version.should.be.equal( version + 1 );
					} );
				} );
			} );

			it( "rejects to fetch upgrade handler used on connecting with database before", () => {
				return Database.open().then( db => {
					( () => { const a = db.upgradeHandler; } ).should.throw(); // eslint-disable-line no-unused-vars
				} );
			} );

			describe( "has method drop() which", () => {
				it( "can be invoked without arguments", () => {
					return Database.open().then( db => db.drop() );
				} );

				it( "removes database", () => {
					return Database.open( "custom-db" )
						.then( db => db.drop() );
						// does not work in Firefox and Edge
						// .then( () => Database.getDatabaseVersion( "custom-db" ) )
						// .then( version => version.should.be.NaN() );
				} );

				it( "succeeds w/o action when database has been removed from cache intermittently", () => {
					return Database.open( "custom-db" )
						.then( db => {
							const saved = Database._cache.read( db.name );
							Database._cache.remove( db.name );

							return db.drop().should.be.Promise().which.is.resolved()
								.then( () => {
									Database._cache.write( db.name, saved );
								} );
						} );
				} );

				it( "delivers same promise on multiple invocations", () => {
					return Database.open( "custom-db" )
						.then( db => {
							const a = db.drop();
							const b = db.drop();

							a.should.be.equal( b );
							return a.should.be.Promise().which.is.resolved();
						} );
				} );
			} );

			describe( "has method hasObjectStore() which", () => {
				it( "requires provision of one argument", () => {
					return Database.open( "testing-local-stores" ).then( db => {
						db.hasObjectStore.should.be.Function().which.has.length( 1 );
					} );
				} );

				it( "detects store to exist in current database", () => {
					return Database.open( "testing-local-stores" )
						.then( db => db.drop() )
						.then( () => Database.open( "testing-local-stores", event => {
							event.target.result.createObjectStore( "created-store" );
						} ) )
						.then( db => {
							db.hasObjectStore( "created-store", db.connection ).should.be.true();
							db.hasObjectStore( "created-stor", db.connection ).should.be.false();
							db.hasObjectStore( "created-store1", db.connection ).should.be.false();
						} );
				} );
			} );
		} );
	} );

	describe( "has static method hasObjectStore() which", () => {
		it( "requires provision of two arguments", () => {
			Database.hasObjectStore.should.be.Function().which.has.length( 2 );
		} );

		it( "requires provision of IDBDatabase instance in second argument", () => {
			( () => Database.hasObjectStore( "test", false ) ).should.throw();
			( () => Database.hasObjectStore( "test", true ) ).should.throw();
			( () => Database.hasObjectStore( "test", "" ) ).should.throw();
			( () => Database.hasObjectStore( "test", "test" ) ).should.throw();
			( () => Database.hasObjectStore( "test", 0 ) ).should.throw();
			( () => Database.hasObjectStore( "test", 1 ) ).should.throw();
			( () => Database.hasObjectStore( "test", () => {} ) ).should.throw();
			( () => Database.hasObjectStore( "test", [] ) ).should.throw();
			( () => Database.hasObjectStore( "test", ["test"] ) ).should.throw();
			( () => Database.hasObjectStore( "test", {} ) ).should.throw();
		} );

		it( "accepts null instead of IDBDatabase instance causing any test for a store to fail", () => {
			Database.hasObjectStore( "test", undefined ).should.be.false();
			Database.hasObjectStore( "test", null ).should.be.false();
		} );

		it( "detects store to exist in a database", () => {
			return Database.open( "testing-stores" )
				.then( db => db.drop() )
				.then( () => Database.open( "testing-stores", event => {
					event.target.result.createObjectStore( "created-store" );
				} ) )
				.then( db => {
					Database.hasObjectStore( "created-store", db.connection ).should.be.true();
					Database.hasObjectStore( "created-stor", db.connection ).should.be.false();
					Database.hasObjectStore( "created-store1", db.connection ).should.be.false();
				} );
		} );
	} );

	// doesn't work in Firefox and Chrome
	describe.skip( "has static method getDatabaseVersion() which", () => {
		it( "requires provision of one argument", () => {
			Database.getDatabaseVersion.should.be.Function().which.has.length( 1 );
		} );

		it( "returns a promise", () => {
			return Database.getDatabaseVersion( "some-database" ).should.be.Promise().which.is.resolved();
		} );

		it( "delivers version of existing database named in first argument", () => {
			return Database.open( "some-database" )
				.then( () => Database.getDatabaseVersion( "some-database" ) )
				.then( version => {
					version.should.not.be.NaN().and.greaterThan( 0 );
				} );
		} );
	} );
} );
