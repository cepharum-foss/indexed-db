import { Database, Store } from "../../src/index.js";

describe( "Class Store", () => {
	it( "uses same cache as class Database", () => {
		Store._cache.should.be.equal( Database._cache );
	} );

	describe( "has a constructor which", () => {
		it( "requires first argument to be instance of Database", function() {
			( () => new Store( undefined, "default" ) ).should.throw();
			( () => new Store( null, "default" ) ).should.throw();
			( () => new Store( false, "default" ) ).should.throw();
			( () => new Store( true, "default" ) ).should.throw();
			( () => new Store( "default", "default" ) ).should.throw();
			( () => new Store( 1, "default" ) ).should.throw();
			( () => new Store( () => "default", "default" ) ).should.throw();
			( () => new Store( ["default"], "default" ) ).should.throw();
			( () => new Store( { name: "default" }, "default" ) ).should.throw();

			return Database.open( "default" )
				.then( db => new Store( db, "default" ) )
				.then( store => store.drop() );
		} );

		it( "requires second argument to be non-empty name of store to manage as string", () => {
			return Database.open( "default" )
				.then( db => {
					( () => new Store( db, undefined ) ).should.throw();
					( () => new Store( db, null ) ).should.throw();
					( () => new Store( db, false ) ).should.throw();
					( () => new Store( db, true ) ).should.throw();
					( () => new Store( db, "" ) ).should.throw();
					( () => new Store( db, 1 ) ).should.throw();
					( () => new Store( db, () => "default" ) ).should.throw();
					( () => new Store( db, ["default"] ) ).should.throw();
					( () => new Store( db, { name: "default" } ) ).should.throw();

					const store = new Store( db, "default" );

					return store.drop();
				} );
		} );
	} );

	describe( "has method open() which", () => {
		it( "can be invoked without argument", () => {
			return Store.open().should.be.Promise().which.is.resolved()
				.then( result => result.should.be.instanceOf( Store ) );
		} );

		it( "can be invoked with one argument", () => {
			return Store.open( "different-db" ).should.be.Promise().which.is.resolved()
				.then( result => result.should.be.instanceOf( Store ) );
		} );

		it( "can be invoked with two arguments", () => {
			return Store.open( "different-db", "special-store" ).should.be.Promise().which.is.resolved()
				.then( result => result.should.be.instanceOf( Store ) );
		} );

		it( "rejects on providing invalid store name in second argument", () => {
			return Store.open( "different-db", "" ).should.be.Promise().which.is.rejected()
				.then( () => Store.open( "different-db", null ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", false ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", true ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", 0 ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", 1 ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", [] ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", ["test"] ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", {} ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", { name: "test" } ).should.be.Promise().which.is.rejected() )
				.then( () => Store.open( "different-db", () => "test" ).should.be.Promise().which.is.rejected() );
		} );

		it( "always provides same promise when invoked without argument", () => {
			return Promise.all( [
				Store.open(),
				Store.open(),
				Store.open(),
			] )
				.then( ( [ a, b, c ] ) => {
					a.should.be.equal( b );
					b.should.be.equal( c );
				} );
		} );

		it( "always provides same promise when invoked with same sole argument", () => {
			return Promise.all( [
				Store.open( "different-db" ),
				Store.open( "different-db" ),
				Store.open( "different-db" ),
			] )
				.then( ( [ a, b, c ] ) => {
					a.should.be.equal( b );
					b.should.be.equal( c );
				} );
		} );

		it( "provides different promises when invoked with different sole argument", () => {
			return Promise.all( [
				Store.open( "different-db-1" ),
				Store.open( "different-db-2" ),
				Store.open( "different-db-3" ),
			] )
				.then( ( [ a, b, c ] ) => {
					a.should.not.be.equal( b );
					b.should.not.be.equal( c );
					c.should.not.be.equal( a );
				} );
		} );

		it( "always provides same promise when invoked with same pair of arguments", () => {
			return Promise.all( [
				Store.open( "different-db", "another-store" ),
				Store.open( "different-db", "another-store" ),
				Store.open( "different-db", "another-store" ),
			] )
				.then( ( [ a, b, c ] ) => {
					a.should.be.equal( b );
					b.should.be.equal( c );
				} );
		} );

		it( "provides different promises when invoked with different pairs of arguments", () => {
			return Promise.all( [
				Store.open( "different-db-1", "another-store" ),
				Store.open( "different-db-2", "another-store" ),
				Store.open( "different-db-3", "another-store" ),
				Store.open( "different-db", "another-store-1" ),
				Store.open( "different-db", "another-store-2" ),
				Store.open( "different-db", "another-store-3" ),
			] )
				.then( ( [ a, b, c, d, e, f ] ) => {
					a.should.not.be.equal( b );
					b.should.not.be.equal( c );
					c.should.not.be.equal( d );
					d.should.not.be.equal( e );
					e.should.not.be.equal( f );
					f.should.not.be.equal( a );
				} );
		} );

		describe( "promises instance for accessing store which", () => {
			describe( "has method clear() which", () => {
				it( "can be invoked without arguments", () => {
					return Store.open().then( store => store.clear() );
				} );
			} );

			describe( "has method drop() which", () => {
				it( "can be invoked without arguments", () => {
					return Store.open().then( store => store.drop() );
				} );

				it( "removes managed store", () => {
					return Store.open( "custom-db", "some-store" )
						.then( store => {
							store.database.hasObjectStore( "some-store" ).should.be.true();

							return store.drop()
								.then( () => {
									store.database.hasObjectStore( "some-store" ).should.be.false();
								} );
						} );
				} );
			} );

			describe( "has method read() which", () => {
				it( "requires provision of one argument", () => {
					return Store.open().then( store => {
						store.read.should.be.Function().which.has.length( 1 );
					} );
				} );

				it( "can be used with two arguments", () => {
					const fallback = { my: "default" };

					return Store.open()
						.then( store => store.read( "my-key", fallback ) )
						.then( value => {
							value.should.be.equal( fallback );
						} );
				} );
			} );

			describe( "has method write() which", () => {
				it( "requires provision of two arguments", () => {
					return Store.open().then( store => {
						store.write.should.be.Function().which.has.length( 2 );
					} );
				} );

				it( "stores keyed value in managed store", () => {
					const value = {
						someObject: ["its value"],
					};
					const fallback = {
						theFallback: "with value"
					};

					return Store.open().then( store => {
						return store.read( "my-key", fallback )
							.then( v => {
								v.should.be.equal( fallback );

								return store.write( "my-key", value );
							} )
							.then( () => store.read( "my-key", fallback ) )
							.then( v => {
								v.should.not.be.equal( fallback );
								v.should.be.deepEqual( value );
							} );
					} );
				} );
			} );

			describe( "has method remove() which", () => {
				it( "requires provision of one argument", () => {
					return Store.open().then( store => {
						store.remove.should.be.Function().which.has.length( 1 );
					} );
				} );

				it( "removes keyed value from managed store", () => {
					const value = {
						someObject: ["its value"],
					};
					const fallback = {
						theFallback: "with value"
					};

					return Store.open().then( store => {
						return store.write( "my-key", value )
							.then( () => store.read( "my-key", fallback ) )
							.then( v => {
								v.should.not.be.equal( fallback );
								v.should.be.deepEqual( value );

								return store.remove( "my-key" );
							} )
							.then( () => store.read( "my-key", fallback ) )
							.then( v => {
								v.should.be.equal( fallback );
							} );
					} );
				} );
			} );
		} );
	} );
} );
