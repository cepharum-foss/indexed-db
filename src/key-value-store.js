/**
 * (c) 2018 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { Store } from "./store.js";

/**
 * Manages special kind of store in a IndexedDB instance of current browser
 * consisting of key-value pairs, only.
 */
class KeyValueStore extends Store {
	/**
	 * Open simple store consisting of key-value pairs in selected database.
	 *
	 * @param {string} dbName name of database to open
	 * @param {string} storeName name of store in database to open
	 * @returns {Promise<Store>} promises manager for accessing store
	 */
	static open( dbName = "default", storeName = "default" ) {
		return super.open( dbName, storeName, {
			key: {
				primary: true,
			},
			value: {}
		} );
	}

	/** @inheritDoc */
	read( key, valueIfMissing = null ) {
		return super.read( key )
			.then( record => {
				return record.hasOwnProperty( "value" ) ? record.value : valueIfMissing;
			} );
	}

	/**
	 * Writes value into storage using provided key.
	 *
	 * @param {string} key identifier of written value in storage
	 * @param {*} value value to write
	 * @return {Promise<Store>} promises current storage after writing value
	 */
	write( key, value ) {
		return super.write( null, { key, value } );
	}

	/** @inheritDoc */
	adjust( key, callback ) {
		return super.adjust( key, record => Promise.resolve( callback( record.value, key ) )
			.then( value => ( value == null ? null : { key, value } ) ) );
	}

	/** @inheritDoc */
	list( ...args ) {
		return super.list( ...args )
			.then( ( { items, total } ) => {
				const numItems = items.length;

				for ( let i = 0; i < numItems; i++ ) {
					const { record } = items[i];

					items[i] = {
						key: record.key,
						value: record.value,
					};
				}

				return total > -1 ? { items, total } : { items };
			} );
	}
}

export { KeyValueStore };
