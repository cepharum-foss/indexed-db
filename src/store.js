/**
 * (c) 2018 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { Database } from "./database.js";
import { Cache } from "./cache.js";

/**
 * @typedef {object} StoreSchemaIndexedProperty
 * @property {boolean} [primary] indicates if current property is to be used as primary key of records
 * @property {boolean} [unique] indicates if index of current property is unique thus rejecting multiple records sharing same value in this property
 */

/**
 * @typedef {object<string,StoreSchemaIndexedProperty>} StoreSchema
 */

/**
 * Manages simple object store in an IndexedDB of current browser consisting of
 * key-value pairs, only.
 *
 * @see RecordStore for a more sophisticated store of records.
 */
class Store {
	/**
	 * @param {Database} database database to contain selected store
	 * @param {string} storeName name of store in database to manage
	 * @param {StoreSchema} schema description of properties per record and indices to use with store
	 * @throws Error on missing mature support for IndexedDB in browser
	 */
	constructor( database, storeName, schema = {} ) {
		if ( !( database instanceof Database ) ) {
			throw new TypeError( "invalid database" );
		}

		if ( typeof storeName !== "string" || storeName.trim().length === 0 ) {
			throw new TypeError( "invalid store name" );
		}

		if ( !schema || typeof schema !== "object" || schema.constructor !== Object ) {
			throw new TypeError( "invalid schema" );
		}

		const primary = Object.keys( schema )
			.map( name => ( ( schema[name] || {} ).primary ? name : null ) )
			.filter( i => i != null );

		if ( primary.length > 1 ) {
			console.warn( "Multi-dimensional primary keys are known to cause trouble in Edge browser." );
		}

		// upgrade database to contain desired object store
		database.upgradeHandler = ( event, db, transaction ) => {
			let store;

			if ( Database.hasObjectStore( storeName, db ) ) {
				store = transaction.objectStore( storeName );
			} else {
				store = db.createObjectStore( storeName, primary.length ? { keyPath: primary.length === 1 ? primary[0] : primary } : { autoIncrement: true } );
			}

			Object.keys( schema )
				.forEach( propertyName => {
					const definition = schema[propertyName] || {};
					const indexName = propertyName;

					if ( !definition.primary && !store.indexNames.contains( indexName ) ) {
						store.createIndex( indexName, propertyName, { unique: definition.unique } );
					}
				} );
		};


		Object.defineProperties( this, {
			/**
			 * Provides database containing managed store.
			 *
			 * @name Store#database
			 * @property {Database}
			 * @readonly
			 */
			database: { value: database },

			/**
			 * Exposes name of store managed by current instance.
			 *
			 * @name Store#name
			 * @property {string}
			 * @readonly
			 */
			name: { value: storeName.trim() },

			/**
			 * Exposes schema definition.
			 *
			 * @name Store#schema
			 * @property {StoreSchema}
			 * @readonly
			 */
			schema: { value: schema },

			/**
			 * Exposes properties of a record's value marked to form its primary
			 * key.
			 *
			 * This property is either nullish or some non-empty list of
			 * properties.
			 *
			 * @name Store#primary
			 * @property {null|Array<string>}
			 * @readonly
			 */
			primary: { value: primary.length ? primary : null },
		} );
	}

	/**
	 * Open store in selected database.
	 *
	 * @param {string} dbName name of database to open
	 * @param {string} storeName name of store in database to open
	 * @param {StoreSchema} schema description of indexed properties per record in store
	 * @returns {Promise<Store>} promises manager for accessing prepared store
	 */
	static open( dbName = "default", storeName = "default", schema = {} ) {
		const _storeName = typeof storeName === "string" ? storeName.trim() : "";
		if ( _storeName === "" ) {
			return Promise.reject( new Error( "invalid store name" ) );
		}

		if ( !schema || typeof schema !== "object" || schema.constructor !== Object ) {
			throw new TypeError( "invalid schema" );
		}

		return Database.open( dbName )
			.then( db => {
				const cache = Cache.read( db.name );
				let store = cache.stores.get( _storeName );

				if ( !store || store.database !== db ) {
					store = new this( db, _storeName, schema );

					cache.stores.set( _storeName, store );
				}

				return store.database.ready.then( () => store );
			} );
	}

	/**
	 * Normalizes provided key or extracts key from provided record.
	 *
	 * @param {?(any[]|any)} key values per primary-key properties, or just the value in case of single-property primary key, omit for extracting from record
	 * @param {object<string,*>} record set of properties key is meant to address in store
	 * @returns {any[]} normalized key
	 * @protected
	 */
	_normalizeKey( key, record = null ) {
		const keySize = this.primary ? this.primary.length : 0;
		let _key;

		if ( Array.isArray( key ) ) {
			if ( !keySize ) {
				// schema lacks definition of any primary-key property, thus keys are numeric auto-incremented values
				throw new TypeError( "key must not be an array, but scalar numeric value" );
			}

			_key = key;
		} else {
			if ( !keySize ) {
				return key;
			}

			if ( key ) {
				_key = [key];
			} else {
				if ( !record ) {
					throw new TypeError( "missing key" );
				}

				_key = new Array( keySize );

				for ( let i = 0; i < keySize; i++ ) {
					const k = this.primary[i];
					if ( !record.hasOwnProperty( k ) ) {
						throw new TypeError( "provided record lacks value for primary-key property %s", k );
					}

					_key[i] = record[k];
				}
			}
		}

		if ( _key.length !== keySize ) {
			throw new TypeError( "invalid size of provided key" );
		}

		return this._checkNormalizedKey( _key );
	}

	/**
	 * Validates segments of provided key.
	 *
	 * @throws TypeError on invalid segments found in provided key
	 * @param {Array<any>} key normalized key to check
	 * @returns {Array<any>} provided key on success
	 * @private
	 */
	_checkNormalizedKey( key ) {
		const numSegments = key.length;

		for ( let i = 0; i < numSegments; i++ ) {
			const segment = key[i];

			switch ( typeof segment ) {
				case "object" :
					if ( segment instanceof Date && !isNaN( segment ) ) {
						break;
					}

					if ( Array.isArray( segment ) && segment !== key ) {
						this._checkNormalizedKey( segment );
						break;
					}

					throw new TypeError( `invalid object in segment #${i + 1} of key [${key.join( ", " )}]` );

				case "number" :
					if ( !isNaN( segment ) ) {
						break;
					}

					throw new TypeError( `invalid NaN in segment #${i + 1} of key [${key.join( ", " )}]` );

				case "string" :
					break;

				default :
					throw new TypeError( `invalid segment #${i + 1} of key [${key.join( ", " )}]` );
			}
		}

		return key;
	}

	/**
	 * Injects provided key's properties into given record if schema names
	 * properties to form inline key.
	 *
	 * @param {any[]} normalizedKey normalized key
	 * @param {object<string,*>} record record to be qualified
	 * @returns {object<string,*>} provided record after probable qualification
	 * @protected
	 */
	_injectKey( normalizedKey, record ) {
		const { primary } = this;

		if ( primary && normalizedKey ) {
			const numPrimaries = primary.length;

			for ( let i = 0; i < numPrimaries; i++ ) {
				record[primary[i]] = normalizedKey[i];
			}
		}

		return record;
	}

	/**
	 * Writes value into storage using provided key.
	 *
	 * @param {string|string[]} key identifier of written value in storage
	 * @param {object} record set of properties to store in association with given key
	 * @return {Promise<Store>} promises current storage after writing value
	 */
	write( key, record ) {
		return this.database.ready
			.then( db => new Promise( ( resolve, reject ) => {
				const storeName = this.name;
				const transaction = db.connection.transaction( storeName, "readwrite" );

				transaction.onerror = reject;

				const store = transaction.objectStore( storeName );
				const normalizedKey = this._normalizeKey( key, record );
				let request;

				const { primary } = this;
				if ( primary ) {
					request = store.put( this._injectKey( normalizedKey, record ) );
				} else {
					switch ( typeof normalizedKey ) {
						case "string" :
						case "number" :
							request = store.put( record, normalizedKey.length === 1 ? normalizedKey[0] : normalizedKey );
							break;

						default :
							throw new TypeError( "invalid type of key" );
					}
				}

				request.onerror = reject;
				request.onsuccess = () => {
					if ( transaction.commit ) {
						transaction.commit();
					}

					resolve( this );
				};
			} ) );
	}

	/**
	 * Fetches value from storage selected by its identifier.
	 *
	 * @param {string} key identifier of value in storage
	 * @param {*} valueIfMissing value to provide if selected key is missing
	 * @returns {Promise<*>} promises value read from database, given fallback if missing
	 */
	read( key, valueIfMissing = null ) {
		return this.database.ready
			.then( db => new Promise( ( resolve, reject ) => {
				const storeName = this.name;
				const transaction = db.connection.transaction( storeName, "readonly" );

				transaction.onerror = reject;

				const normalizedKey = this._normalizeKey( key );
				const store = transaction.objectStore( storeName );
				const request = store.get( normalizedKey.length === 1 ? normalizedKey[0] : normalizedKey );

				request.onerror = error => {
					transaction.abort();
					reject( error );
				};

				request.onsuccess = event => {
					const { result } = event.target;

					resolve( result == null ? valueIfMissing : result );
				};
			} ) );
	}

	/**
	 * Fetches record selected by its ID and invokes provided callback for
	 * adjusting fetched record before writing back to store.
	 *
	 * Provided callback may return `null` to request removal of provided
	 * record from store.
	 *
	 * @note The whole cycle of reading record before invoking callback and
	 *       writing it back to database isn't guaranteed to be covered by
	 *       single transaction. A synchronous callback is expected to keep the
	 *       transaction used to read the record before, though.
	 *
	 * @param {string} key identifier of record to adjust
	 * @param {function(record:object, key:string):(object|Promise<object>)} callback function invoked for returning adjusted version of provided record
	 * @returns {Promise<Store>} promises record adjusted
	 */
	adjust( key, callback ) {
		return this.database.ready
			.then( db => new Promise( ( resolve, reject ) => {
				const storeName = this.name;
				const transaction = db.connection.transaction( storeName, "readwrite" );

				transaction.onerror = reject;

				const store = transaction.objectStore( storeName );
				const normalizedKey = this._normalizeKey( key );
				const read = store.get( normalizedKey.length === 1 ? normalizedKey[0] : normalizedKey );

				read.onerror = reject;
				read.onsuccess = event => {
					const record = event.target.result;

					Promise.resolve( callback( record, normalizedKey ) )
						.then( adjusted => {
							let write;

							if ( adjusted == null ) {
								write = store.delete( normalizedKey );
							} else if ( this.primary ) {
								write = store.put( this._injectKey( normalizedKey, adjusted ) );
							} else {
								write = store.put( adjusted, normalizedKey.length === 1 ? normalizedKey[0] : normalizedKey );
							}

							write.onerror = reject;
							write.onsuccess = () => resolve( this );
						} )
						.catch( reject );
				};
			} ) );
	}

	/**
	 * Removes value from storage selected by its identifier.
	 *
	 * @param {any|any[]} key identifier of value in storage
	 * @returns {Promise<Store>} promises current store after removing selected value
	 */
	remove( key ) {
		return this.database.ready
			.then( db => new Promise( ( resolve, reject ) => {
				const name = this.name;
				const transaction = db.connection.transaction( name, "readwrite" );

				transaction.onerror = reject;

				const normalizedKey = this._normalizeKey( key );
				const store = transaction.objectStore( name );
				const request = store.delete( normalizedKey.length === 1 ? normalizedKey[0] : normalizedKey );

				request.onerror = error => {
					transaction.abort();
					reject( error );
				};

				request.onsuccess = () => {
					if ( transaction.commit ) {
						transaction.commit();
					}

					resolve( this );
				};
			} ) );
	}

	/**
	 * Removes all records from managed store.
	 *
	 * @returns {Promise<Store>} promises all records in managed store removed
	 */
	clear() {
		return this.database.ready
			.then( db => new Promise( ( resolve, reject ) => {
				const name = this.name;
				const transaction = db.connection.transaction( name, "readwrite" );

				transaction.onerror = reject;

				const request = transaction.objectStore( name ).clear();

				request.onerror = error => {
					transaction.abort();
					reject( error );
				};

				request.onsuccess = () => {
					if ( transaction.commit ) {
						transaction.commit();
					}

					resolve( this );
				};
			} ) );
	}

	/**
	 * Removes managed store from database.
	 *
	 * @returns {Promise} promises managed store removed
	 */
	drop() {
		return this.database.ready
			.then( () => {
				this.database.upgradeHandler = event => {
					event.target.result.deleteObjectStore( this.name );
				};

				return this.database.ready;
			} )
			.then( () => {
				const cache = Cache.read( this.database.name );
				if ( cache && cache.stores ) {
					cache.stores.delete( this.name );
				}
			} );
	}

	/**
	 * Fetches number of records in current store optionally limited to those
	 * matching provided range.
	 *
	 * @param {IDBKeyRange} range optional range of records to count only
	 * @param {String} indexName name of index to apply range definition for matching records
	 * @returns {Promise<int>} number of (matching) records
	 */
	count( range = null, indexName = null ) {
		return this.database.ready
			.then( db => new Promise( ( resolve, reject ) => {
				const store = db.connection.transaction( this.name, "readonly" ).objectStore( this.name );

				let source;

				if ( indexName ) {
					source = store.index( indexName );
				} else {
					source = store;
				}

				const request = source.count( range );

				request.onerror = reject;
				request.onsuccess = () => resolve( request.result );
			} ) );
	}

	/**
	 * Lists records in store.
	 *
	 * @param {int} offset number of records to skip on listing
	 * @param {int} limit maximum number of records to retrieve
	 * @param {boolean} count set true to get total number of (matching) records as well
	 * @param {?string} indexName name of indexed property to check per record
	 * @param {?IDBKeyRange} range description of range to iterate over
	 * @param {?boolean} ascending set true for iterating in ascending order (according to selected index), false for iterating in descending order
	 * @param {boolean} distinct set true to skip additional occurrences of same index value when iterating
	 * @returns {Promise<{items: Array<{key:*, record:object}>, total:?int}>} promises list of records read from store
	 */
	list( {
		offset = 0, limit = Infinity, count = false,
		indexName = null, range = null,
		ascending = null, distinct = false,
	} = {} ) {
		if ( indexName && !this.schema[indexName] ) {
			return Promise.reject( new TypeError( "invalid use of unknown index name" ) );
		}

		return this.database.ready
			.then( db => {
				const store = db.connection.transaction( this.name, "readonly" ).objectStore( this.name );
				const source = indexName == null ? store : store.index( indexName );

				const promises = [
					new Promise( ( resolve, reject ) => {
						const dir = ascending == null ? "next" : ascending ? "next" : "prev";
						const mode = distinct ? "unique" : "";
						const request = source.openCursor( range, dir + mode );
						const collected = [];
						let _offset = parseInt( offset ) || 0;
						let _limit = parseInt( limit ) || Infinity;

						request.onerror = reject;
						request.onsuccess = event => {
							const cursor = event.target.result;

							if ( cursor ) {
								if ( _offset-- < 1 ) {
									const { primaryKey: key } = cursor;

									collected.push( {
										key: Array.isArray( key ) && key.length === 1 ? key[0] : key,
										record: cursor.value,
									} );

									if ( --_limit < 1 ) {
										cursor.advance( 0xfffffff );
										return;
									}
								}

								cursor.continue();
							} else {
								resolve( collected );
							}
						};
					} ),
				];

				if ( count ) {
					promises.push( new Promise( ( resolve, reject ) => {
						const request = range == null ? source.count() : source.count( range );

						request.onerror = reject;
						request.onsuccess = () => resolve( request.result );
					} ) );
				}

				return Promise.all( promises )
					.then( ( [ items, total ] ) => ( count ? { items, total } : { items } ) );
			} );
	}

	/**
	 * Lists records in store with indexed property per record exactly matching
	 * given value.
	 *
	 * @param {string} indexName name of indexed property to check per record
	 * @param {*} value value to be found in selected property of matching records
	 * @param {int} offset number of records to skip on listing
	 * @param {int} limit maximum number of records to retrieve
	 * @param {boolean} count set true to get total number of (matching) records as well
	 * @returns {Promise<{items: Array<{key:*, record:object}>, total:?int}>} promises list of records read from store
	 */
	findMatching( indexName, value, {
		offset = 0, limit = Infinity, count = false
	} = {} ) {
		return this.list( { indexName, range: IDBKeyRange.only( value ), offset, limit, count } );
	}

	/**
	 * Lists records in store with indexed property per record having value in
	 * given range of values.
	 *
	 * @param {string} indexName name of indexed property to check per record
	 * @param {?*} lower optional lower bound of range of values, range is always including any value given here
	 * @param {?*} upper optional upper bound of range of values, range is always including any value given here
	 * @param {int} offset number of records to skip on listing
	 * @param {int} limit maximum number of records to retrieve
	 * @param {boolean} count set true to get total number of (matching) records as well
	 * @param {?boolean} ascending set true for iterating in ascending order (according to selected index), false for iterating in descending order
	 * @param {boolean} distinct set true to skip additional occurrences of same index value when iterating
	 * @returns {Promise<{items: Array<{key:*, record:object}>, total:?int}>} promises list of records read from store
	 */
	findInRange( indexName, lower, upper = null, {
		offset = 0, limit = Infinity, count = false,
		ascending = null, distinct = false
	} = {} ) {
		let range;

		if ( lower == null && upper == null ) {
			range = null;
		} else if ( lower == null ) {
			range = IDBKeyRange.upperBound( upper );
		} else if ( upper == null ) {
			range = IDBKeyRange.lowerBound( lower );
		} else {
			range = IDBKeyRange.bound( lower, upper );
		}

		return this.list( { indexName, range, offset, limit, count, ascending, distinct } );
	}
}

Object.defineProperties( Store, {
	/**
	 * Exposes common runtime cache.
	 *
	 * @name Database._cache
	 * @property {Map<string,object>}
	 * @readonly
	 * @protected
	 */
	_cache: { value: Cache },
} );

export { Store };
