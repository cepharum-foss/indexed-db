const path = require( "path" );

module.exports = {
	// mode: "production",
	mode: "none",
	entry: "./src/index.js",
	output: {
		path: path.resolve( __dirname, "dist" ),
		filename: "indexed-db.umd.js",
		library: "IndexedDb",
		libraryTarget: "umd",
	},
	target: "web",
};
